using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SaveSystem;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames;
using System.Text;
using System;


public class PGS_Manager : MonoBehaviour
{
    public delegate void OnAuthorization(); //This Delegate Void is executed from GameFlow
    public OnAuthorization onAuthorization;

    public void InitPGSConfig(){
        PlayGamesClientConfiguration configuration = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(configuration);
        PlayGamesPlatform.Activate();
    }

    public void UserSignIn(){
        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, (result) => {    //PromptAlways for manual login
            //CustomDebug.Instance.Log(result);
           onAuthorization.Invoke();
           //Saved the method from GameFlow.cs in this delegate for more flexability, and decoupling of the "Login" Methods
        });
    }

    private void UserSignOut(){
        PlayGamesPlatform.Instance.SignOut();
    }

    //Method used for UI-Buttons
    public void ManualSignIn(){
        UserSignIn();
    }

    //Method used for UI-Buttons
    public void ManualSignOut(){
        UserSignOut();
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Here starts saved games stuff. Yes, the coupling here was inappropriate and may be changed, If i find the time to do it.
#region Saved_Games

    //Helper methods found in the Presentations
    public byte[] JsonToBinary(string input){
        return Encoding.ASCII.GetBytes(input);
    }
    public string BinaryToJson(byte[] bytes){
        return Encoding.ASCII.GetString(bytes);
    }

    //This is called on awake in GameFlow
    public void EnablePGSSavedGames(){
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
        PlayGamesPlatform.InitializeInstance(config);
    }
    //Helpers End

    //Opens native UI
    public void OpenSaveGameUI(){
        try{
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;    //This returns always null
            savedGameClient.ShowSelectSavedGameUI("Select saved game", 10, true, true, OnSaveGameSelected);
        }
        catch{
            //CustomDebug.Instance.Log("Could not get instance of savedGameClient");
            //No save game found
            
            //If this try/catch is left out, it will always return null while retrieving hte ISavedGameClient interface.
        }
    }

    //saving metadata as a variable to pass it to different methods
    private ISavedGameMetadata metadata;
    private void OnSaveGameSelected(SelectUIStatus status, ISavedGameMetadata game){
        try{
            metadata = game;
        }
        catch{
            //When no savegame is available, the try catch is here to avoid nullref-exceptions
        }

        if(status == SelectUIStatus.SavedGameSelected){
            OpenSavedGame(game.Filename);
        }
    }

    public void OpenSavedGame(string filename){
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, OnSaveGameOpened);
    }

    public void OnSaveGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game){

        //here the instance of ISavedGameMetadata is retrieved

        if(status == SavedGameRequestStatus.Success){
            //handle reading or writing of saved game
            LoadGame();
        }
        else{
            //handle error
        }
    }

    public void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game){
        if(status == SavedGameRequestStatus.Success){
            //handle reading or writing of saved game
        }
        else{
            //handle error
        }
    }

    //Get a screenshot
    public Texture2D GetScreenShot(){
        Texture2D screenshot = new Texture2D(1024, 700);   
        screenshot.ReadPixels(new Rect(0, 0, Screen.width, (Screen.width/1024)*700), 0, 0);
        return screenshot;
    }

    public void ExecuteSaveGame(){
     string jsonString = JsonUtility.ToJson(SaveManager.SaveData); 
     byte[] stringAsByteArr = JsonToBinary(jsonString); 
     SaveGame(metadata, stringAsByteArr, CustomTimeline.Instance.CalculatePlayTime(DateTime.Now));  
    }

    public void LoadGame(){
        LoadGameData(metadata);
    }

    //Save the game
    void SaveGame(ISavedGameMetadata gameMetadata, byte[] savedData, TimeSpan totalPlayTime){
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
        builder = builder.WithUpdatedPlayedTime(totalPlayTime).WithUpdatedDescription("Saved game at " + DateTime.Now);

        Texture2D savedImage = GetScreenShot(); 

        if(savedImage != null){
            byte[] pngData = savedImage.EncodeToPNG();
            builder = builder.WithUpdatedPngCoverImage(pngData);
        }

        SavedGameMetadataUpdate updateMetaData = builder.Build();
        savedGameClient.CommitUpdate(gameMetadata, updateMetaData, savedData, OnSavedGameWritten);
    }

    //Load the game
    void LoadGameData(ISavedGameMetadata game){
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
    }

    public void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data){
        if(status == SavedGameRequestStatus.Success){
            SaveManager.SaveData = JsonUtility.FromJson<SaveData>(BinaryToJson(data));
            // handle processing the byte array data
        }
        else{
            // handle error
        }
    }

    public void DeletaGameData(string filename){
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, DeleteSavedGame);
    }

    public void DeleteSavedGame(SavedGameRequestStatus status, ISavedGameMetadata game){
        if(status == SavedGameRequestStatus.Success){
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.Delete(game);
        }
        else
        {
            //handle error
        }
    }

#endregion
}



//Frage: Nullref wehh IsaveGameClient retrieven. A: Wird im Build später nie null sein!:)
