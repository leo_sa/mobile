using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames.BasicApi;
using GooglePlayGames;
using ScoreSystem;
using System;
using Core; //ref to core to implement this faster as a singleton. Singleton is required to get a reference in GameOverState.

public class Leaderboard : Singleton<Leaderboard>
{
    private const string leaderboardID = GPGSIds.leaderboard_topscore;
    [SerializeField] private LeaderBoardEntry[] leaderBoardEntries;
    
    public void ClassicScoreExecution(){
        Debug.Log(leaderboardID);
        ReportScore(ScoreManager.Instance.HighScore, leaderboardID);
    }
    private void ReportScore(long input, string leaderboardName){
        Debug.Log(input);
        Social.ReportScore(input, leaderboardName, 
        (res) =>{
            ReadLeaderboard(leaderboardID); //Leaderboard can be read as soon as the score is reported
        });       
    }

    public void ShowLeaderboardClassic(){
        PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboardID);
    }

    //Leaderboard ID - the public one: CgkIurXl_owQEAIQAQ
    public void ReadLeaderboard(string leaderboardName){
        PlayGamesPlatform.Instance.LoadScores(leaderboardName, LeaderboardStart.PlayerCentered, 10, LeaderboardCollection.Public, LeaderboardTimeSpan.AllTime, FillData);
    }

    //Uses the same Leaderboard-ID
    public void ReadLeaderboardFriends(string leaderboardName){
        PlayGamesPlatform.Instance.LoadScores(leaderboardName, LeaderboardStart.PlayerCentered, 10, LeaderboardCollection.Social, LeaderboardTimeSpan.AllTime, FillData);
    }

    //Fills all leaderboard-entries with provided Data
    private void FillData(LeaderboardScoreData data){

        for (int i = 0; i < leaderBoardEntries.Length; i++)   //Looping through all leaderboard-entries to show leaderboard data
        {
            try{
                string name = data.Scores[i].userID;
                string rank = data.Scores[i].rank.ToString();
                string score = data.Scores[i].value.ToString();
                leaderBoardEntries[i].FillLeaderBoardEntry(rank, name, score);
            }
            catch{
                //The code above may yield an indexoutofrange exception if no data is found
                string name = "---";
                string rank = "---";
                string score = "---";
                leaderBoardEntries[i].FillLeaderBoardEntry(rank, name, score);
            }
        }

    }

}
