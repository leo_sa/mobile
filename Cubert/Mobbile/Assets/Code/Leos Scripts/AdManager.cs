using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine;
using Core;
using CurrencySystem;

public class AdManager : MonoBehaviour, IUnityAdsListener
{
    [SerializeField] private GameObject adButton;
    public delegate void onEnableAds();
    public onEnableAds onAdsEnabled;
    private const string gameID = "4196985";
    private const bool IsTestMode = true;

    private const string fullLength_Android = "Interstitial_Android", rewardAndroid = "Rewarded_Android";

    //InitAds is called from gameflow.cs
    public void InitAds(){
        adButton.SetActive(true);   //This should be in OnUnityAdsReady BUT (explanation in OnUnityAdsReady)
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameID, IsTestMode);
    }

    public void PlayFullLengthAd(){
        Advertisement.Show(fullLength_Android);
    }

    public void PlayRewardAd(){
        Advertisement.Show(rewardAndroid);
    }

    private void OnDestroy(){
        Advertisement.RemoveListener(this);
    }


    //Interface Related
    public void OnUnityAdsReady(string placementID){    //This gets called as soon as an ad is ready to be watched. This hut doesnt get called. Why.
      //Button is enabled on InitAds, because in the build this method is not executed for some reason. It works without problems in the editor, but the build says no.
    }

    public void OnUnityAdsDidError(string message){
        Debug.LogError("Ad resulted in an error" + message);
    }

    public void OnUnityAdsDidStart(string placementID){

    }

    public void OnUnityAdsDidFinish(string placementID, ShowResult showResult){
        if(showResult == ShowResult.Finished){
            OnReward();
        }
    }

    //Currently the Player gets rewarded with 15 coins
    private void OnReward(){
        CoinPurse.Instance.AddCoinsToTotal(15);
    }


}
