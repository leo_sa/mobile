using System.Collections;
using System.Collections.Generic;
using GooglePlayGames.BasicApi;
using UnityEngine;
using GooglePlayGames;

public class AchievementManager : MonoBehaviour
{
    private static AchievementManager instance;
    public static AchievementManager Instance { get => instance; set => instance = value; }

    //Pseudo-Singleton, many classes require a reference to this instance
    //(Written before I knew of the existence of Core.Singleton<T>)
    private float runTime;  //'literally' the time of the current run

    private void Start(){
        if(instance == null) instance = this;
    }
    private void Update(){
        runTime += Time.deltaTime; //Time needs to be tracked continiously, to unlock achievement at the appropriate moment.
        TimedAchievements();    
    }

    //Helper/general methods for achievements
    #region  Basics
    public void ShowAchievementsUI(){
        Social.ShowAchievementsUI();      
    }
    private void UnlockAchievement(string input){
        Social.ReportProgress(input, 100.0f, (success) => {
            //Achievement was unlocked
        });
    }

    private void IncrementAchievement(string input, int amount){
        PlayGamesPlatform.Instance.IncrementAchievement(input, amount, (success) => {
            //Achievement was incementend
        });
    }
    #endregion

    //Unlock specific Achievements
    #region Specifics

    //"Turbo Avoider" -> Avoid 25 Obstacles in a single run
    public void UnlockTurboAvoider(){
        UnlockAchievement("CgkIurXl_owQEAIQAg");
    }

    //"Player" -> Play 10 runs
    public void IncrementPlayer(){
        IncrementAchievement("CgkIurXl_owQEAIQAw", 1); //"1" because this is meant to be executed, when a new run is started
    }

    //"Greedy" -> Collect 3 coins without being hit
    public void UnlockGreedy(){
        UnlockAchievement("CgkIurXl_owQEAIQBA");
    }

    //"ToTheLeft" Survive 30 seconds by moving only left
    public void UnlockToTheLeft(){
        UnlockAchievement("CgkIurXl_owQEAIQBQ");
    }

    //Split -> Get hit by an axe three times in a row
    public void UnlockSplit(){
        UnlockAchievement("CgkIurXl_owQEAIQBg");
    }

    //"Champion" -> Beat your own highscore
    public void UnlockChampion(){
        UnlockAchievement("CgkIurXl_owQEAIQBw");
    }

    //"Shopping Queen" -> Buy 1 Item from the shop
    public void UnlockShoppingQueen(){
        UnlockAchievement("CgkIurXl_owQEAIQCA");
    }

    //"Fashionista" -> Unlock 1 Hat 
    public void UnlockFashionista(){
        UnlockAchievement("CgkIurXl_owQEAIQCQ");
    }
    
    //"Its Safe inside" -> Use protective field three times in a row
    public void UnlockItsSafeInside(){
        UnlockAchievement("CgkIurXl_owQEAIQCg");
    }

    //"Staying Alive" -> Stay alive 3 minutes in a row
    public void UnlockStayingAlive(){
        UnlockAchievement("CgkIurXl_owQEAIQCw");
    }



    #endregion
    
    #region Trackers

    //TODO: Add flags to not unlock 2 times

    //tracks the avoided obstacles in a single run

    private int avoidedObstacles;
    public void IncreaseAvoidedNumber(){
        avoidedObstacles++;
        if(avoidedObstacles == 25){
            UnlockTurboAvoider();
        }
    }

    //resets all run specific values
    public void ResetRunValues(){
        avoidedObstacles = 0;
        runTime = 0;
        
        EraseCoinStrak();
        EraseAxeHits();
        EraseShieldUsed();
    }

    private int currentCoinStreak;

    //Balacing?
    public void IncreaseCoinStreak(){
        currentCoinStreak++;
        if(currentCoinStreak == 10){
            UnlockGreedy();
        }
    }

    public void EraseCoinStrak(){
        currentCoinStreak = 0;
    }

    private int concurrentAxeHits;
    public void IncreaseAxeHits(){
        concurrentAxeHits++;
        if(concurrentAxeHits == 3){
            UnlockSplit();
        }
    }

    public void EraseAxeHits(){
        concurrentAxeHits = 0;
    }

    private int shieldTracker;
    public void IncreaseShieldUsed(){
        shieldTracker++;
        if(shieldTracker == 3){
            UnlockItsSafeInside();
        }
    }

    public void EraseShieldUsed(){
        shieldTracker = 0;
    }


    private bool hasMovedRight;
    public bool HasMovedRight { get => hasMovedRight; set => hasMovedRight = value; }

    private void TimedAchievements(){

        if(runTime > 30 && !hasMovedRight){
            UnlockToTheLeft();
        }

        if (runTime > 180)  //180Seconds = 3 min
        {
            UnlockStayingAlive();
        }
    }

    #endregion
}
