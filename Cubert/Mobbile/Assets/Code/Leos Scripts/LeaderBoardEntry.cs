using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//Class to handle leaderboard entries
public class LeaderBoardEntry : MonoBehaviour
{
   private TextMeshProUGUI rankText, nameText, scoreText;

   private void Awake(){
       rankText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();      //The Hierarchy is always the same, thus this automation is possible
       nameText = transform.GetChild(1).GetComponent<TextMeshProUGUI>();      
       scoreText = transform.GetChild(2).GetComponent<TextMeshProUGUI>();      
   }

   public void FillLeaderBoardEntry(string rank, string name, string score){
      rankText.text = rank;
      nameText.text = name;
      scoreText.text = score;
   }
}
