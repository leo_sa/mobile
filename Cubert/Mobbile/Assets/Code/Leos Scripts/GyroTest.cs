using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroTest : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float speed = 1;
    // Update is called once per frame
    void Update()
    {
        Vector3 move = GyroMovement(1);
        rb.velocity = new Vector3(GyroMovement(speed).x, rb.velocity.y, rb.velocity.z);
        Debug.Log(move);
    }


    private Vector3 GyroMovement(float speed)
    {
        Vector3 cachedGyro = Vector3.Normalize(Input.gyro.gravity) * speed;
        return cachedGyro;
    }
}
