using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using System;
public class CustomTimeline : Singleton<CustomTimeline>
{
    private DateTime startedTime;
    protected override void Awake(){
        base.Awake();
        startedTime = DateTime.Now;
    }
    private static TimeSpan span;
    public static TimeSpan Span { get => span; set => span = value; }

    public TimeSpan CalculatePlayTime(DateTime stopTime){
        TimeSpan interval = startedTime - stopTime;

        totalPlayTimeSeconds += interval.Seconds;

        TimeSpan finalSpan = TimeSpan.FromSeconds(totalPlayTimeSeconds);

        return finalSpan;
    }

    private float totalPlayTimeSeconds;
    //Load total playtime at start
    private void Start(){
        totalPlayTimeSeconds = PlayerPrefs.GetFloat("totalPlayTimeF");
    }

    //and save totalplaytime at the end, also through the usage of the timespan
    private void OnDisable(){
        totalPlayTimeSeconds = CalculatePlayTime(DateTime.Now).Seconds;
        PlayerPrefs.SetFloat("totalPlayTimeF", totalPlayTimeSeconds);
    }
}
