﻿
using Core;
using GameFlowSystem.States;
using PlayerSystem;
using UnityEngine;

namespace GameFlowSystem
{
	/// <summary>
	/// 	State Machine controlling the game flow. Implements state pattern.
	/// </summary>
	public class GameFlow : Singleton<GameFlow>
	{
		#region Serialize Fields

		[SerializeField] private GameObject[] toDisable;
		[SerializeField] private AdManager adManager;
		[SerializeField] private MainMenuState _mainMenuState;
		[SerializeField] private PauseGameState _pauseGameState;
		[SerializeField] private StartGameState _startGameState;
		[SerializeField] private PlayGameState _playGameState;
		[SerializeField] private GameOverState _gameOverState;
		[SerializeField] private SaveState _saveState;
		[SerializeField] private LoadState _loadState;
		[SerializeField] private PGS_Manager _PGSManager;	//A reference to the googlaPlayGames Manager (incl. login etc.)

		#endregion

		#region Private Fields

		private IGameState _currentState;
		private PlayerController _player;

		#endregion

		#region Properties

		public GameOverState GameOverState => _gameOverState;
		public bool IsGameRunning => _currentState.Equals(PlayGameState);
		public LoadState LoadState => _loadState;
		public MainMenuState MainMenuState => _mainMenuState;
		public PauseGameState PauseGameState => _pauseGameState;
		public PlayerController Player => _player;
		public PlayGameState PlayGameState => _playGameState;
		public SaveState SaveState => _saveState;
		public StartGameState StartGameState => _startGameState;

		private static bool socialPlatformActivated = false;	//bool marked as static to avoid logging in the player every game-restart

		#endregion

		#region Unity methods

		protected override void Awake()
		{
			base.Awake();

			_player = FindObjectOfType<PlayerController>();

			_currentState = LoadState;
			_currentState.StateEnter();

			Time.timeScale = 0f;

//The savedGameclient interface will always be null in the editor for some reason. This shouldnt be the case in the final build...buuuuut it is :|

			_PGSManager.EnablePGSSavedGames();
			//Opening savedgames as provided in the presentation.

			//Initializing Ads
			adManager.InitAds();

			//Authenticate the player
			Authenticate();

		}

		protected void OnDisable(){
			//_PGSManager.ExecuteSaveGame();	//saving the game as soon as the application quits
		}
		
		private void Update()
		{
			if (_currentState == null)
			{
				return;
			}

			// state pattern - update current state, hceck if it needs to be switched. Exit current one and enter new one if required.
			IGameState nextState = _currentState.StateUpdate();
			if (nextState != _currentState)
			{
				_currentState.StateExit();
				_currentState = nextState;
				_currentState.StateEnter();
			}
		}

		//This code will always be executed on Awake
		private void Authenticate(){
			if(!socialPlatformActivated){
				_PGSManager.InitPGSConfig();
				_PGSManager.onAuthorization += SetLoadState;

				_PGSManager.onAuthorization += _PGSManager.OpenSaveGameUI;	//SavedGames are probably account-reliant, so per login a savegame has to be chosen and applied

				_PGSManager.UserSignIn();
				socialPlatformActivated = true;
			}
			else{
				SetLoadState();
			}
		}

		private void SetLoadState(){
			_currentState = LoadState;
			_currentState.StateEnter();
		}

		#endregion
	}
}